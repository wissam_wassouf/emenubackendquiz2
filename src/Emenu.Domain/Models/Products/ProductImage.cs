using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using Volo.Abp.Domain.Entities.Auditing;

namespace Emenu.Models.Products
{
    public class ProductImage : AuditedAggregateRoot<Guid>
    {

        [StringLength(500)]
        public string ImageUrl { get; set; }

        //public int ProductID { get; set; }
        public Guid ProductID { get; set; }

        public virtual Product Product { get; set; }
 
    }

}