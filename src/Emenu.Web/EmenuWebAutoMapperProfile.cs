﻿using AutoMapper;
using Emenu.Products.Dto;
using Emenu.Web.Pages.Products;

namespace Emenu.Web;

public class EmenuWebAutoMapperProfile : Profile
{
    public EmenuWebAutoMapperProfile()
    {
        //Define your AutoMapper configuration here for the Web project.
        CreateMap<AddTranslationModal.ProductTranslationViewModel, AddProductTranslationDto>();

    }
}
