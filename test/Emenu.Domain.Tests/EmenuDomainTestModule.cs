﻿using Emenu.EntityFrameworkCore;
using Volo.Abp.Modularity;

namespace Emenu;

[DependsOn(
    typeof(EmenuEntityFrameworkCoreTestModule)
    )]
public class EmenuDomainTestModule : AbpModule
{

}
