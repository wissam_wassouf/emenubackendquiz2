using Emenu.MultiLingualObjects;
using System;
using Volo.Abp.Domain.Entities;
using Volo.Abp.Domain.Entities.Auditing;

namespace Emenu.Models.Products;

public class ProductTranslation : AuditedAggregateRoot<Guid>, IObjectTranslation
{
    public Guid ProductId { get; set; }

    public string Name { get; set; }

    public string Language { get; set; }

    public override object[] GetKeys()
    {
        return new object[] { ProductId, Language};
    }
}