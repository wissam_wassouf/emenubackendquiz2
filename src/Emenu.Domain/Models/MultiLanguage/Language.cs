﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Collections.Generic;
using System;
using Volo.Abp.Domain.Entities.Auditing;

namespace Emenu.Models.MultiLanguage
{
    public class Language : AuditedAggregateRoot<Guid>
    {
 
        public Language()
        {
            SystemTableFiledLanguages = new HashSet<SystemTableFiledLanguage>();
        }

        [StringLength(50)]
        public string Name { get; set; }

        public virtual ICollection<SystemTableFiledLanguage> SystemTableFiledLanguages { get; set; }

    }
}