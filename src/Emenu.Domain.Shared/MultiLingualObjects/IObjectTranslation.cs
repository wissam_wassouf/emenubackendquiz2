namespace Emenu.MultiLingualObjects;

public interface IObjectTranslation
{
    string Language { get; set; }
}