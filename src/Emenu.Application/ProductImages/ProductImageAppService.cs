﻿using AutoMapper.Internal.Mappers;
using Emenu.CloudinaryServices;
using Emenu.Models.Products;
using Emenu.ProductImages;
using Emenu.ProductImages.Dto;
using Emenu.Products.Dto;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Application.Services;
using Volo.Abp.Domain.Repositories;

namespace Emenu.ProductImages
{


    public class ProductImageAppService : ApplicationService, IProductImageAppService
    {
        private readonly IRepository<ProductImage, Guid> _productImageRepository;
        private readonly ICloudinaryService _cloudinaryService;

        public ProductImageAppService(
            IRepository<ProductImage, Guid> productImageRepository,
            ICloudinaryService cloudinaryService)
        {
            _productImageRepository = productImageRepository;
            _cloudinaryService = cloudinaryService;
        }

        public async Task<ProductImageDto> CreateAsync([FromForm] ProductImageCreateDto input)
        {
            // Upload image to Cloudinary and get the image URL
            string imageUrl = await _cloudinaryService.UploadImageAsync(input.File);

            // Create a new ProductImage entity
            var productImage = new ProductImage
            {
                ImageUrl = imageUrl,
                ProductID = input.ProductID
            };

            // Save the entity
            var newProductImage = await _productImageRepository.InsertAsync(productImage);

            return ObjectMapper.Map<ProductImage, ProductImageDto>(newProductImage);
        }

        public async Task<ProductImageDto> GetAsync(Guid id)
        {
            var productImage = await _productImageRepository.GetAsync(id);
            return ObjectMapper.Map<ProductImage, ProductImageDto>(productImage);
        }

        public async Task<ProductImageDto> UpdateAsync(Guid id, [FromForm] ProductImageUpdateDto input)
        {
            var productImage = await _productImageRepository.GetAsync(id);

            // Delete the existing image from Cloudinary
            await _cloudinaryService.DeleteImageAsync(productImage.ImageUrl);

            // Upload new image to Cloudinary and get the image URL
            string newImageUrl = await _cloudinaryService.UploadImageAsync(input.File);

            // Update the image URL
            productImage.ImageUrl = newImageUrl;
            productImage.ProductID = input.ProductID;

            return ObjectMapper.Map<ProductImage, ProductImageDto>(productImage);
        }

        public async Task DeleteAsync(Guid id)
        {
            var productImage = await _productImageRepository.GetAsync(id);
            if (productImage != null)
            {
                // Delete image from Cloudinary
                await _cloudinaryService.DeleteImageAsync(productImage.ImageUrl);

                // Delete the entity
                await _productImageRepository.DeleteAsync(productImage);
            }
        }


        // Other methods
    }

}
