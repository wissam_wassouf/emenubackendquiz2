﻿using Emenu.MultiLingualObjects;
using Emenu.ProductImages.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using Volo.Abp.Application.Dtos;

namespace Emenu.Products.Dto
{
    public class ProductDto : AuditedEntityDto<Guid> , IObjectTranslation
    {
        public string Name { get; set; }

        public string Description { get; set; }

        public decimal? InventoryNumber { get; set; }

        public decimal? Price { get; set; }

        public decimal? Cost { get; set; }

        public string Code { get; set; }

        public string Language { get;set; }

    public ICollection<ProductImageDto>? ProductImages { get; set; }
     //public ICollection<ProductCategoryDto> ProductCategories { get; set; }
    }
}
