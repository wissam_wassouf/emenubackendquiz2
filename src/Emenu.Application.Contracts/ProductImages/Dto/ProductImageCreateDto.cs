﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;
using Volo.Abp.Application.Dtos;

namespace Emenu.ProductImages.Dto
{
   // public class ProductImageCreateDto : AuditedEntityDto<Guid>
    public class ProductImageCreateDto 
    {
        public Guid ProductID { get; set; }

        public IFormFile File { get; set; }
    }
}
