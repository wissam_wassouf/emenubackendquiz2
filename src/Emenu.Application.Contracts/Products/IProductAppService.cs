﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Emenu.Products.Dto;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Application.Services;

namespace Emenu.Products
{
    public interface IProductAppService : ICrudAppService< //Defines CRUD methods
        ProductDto, //Used to show Products
        Guid, //Primary key of the Product entity
        PagedAndSortedResultRequestDto, //Used for paging/sorting
        CreateUpdateProductDto> //Used to create/update a Product
    {
        Task AddTranslationsAsync(Guid id, AddProductTranslationDto input);// added this line
    }
}
