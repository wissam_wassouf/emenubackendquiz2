using Emenu.Models.Categories;
using Emenu.Models.Products;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics.Contracts;
using System.Text;
using Volo.Abp.Domain.Entities.Auditing;

namespace Emenu.Models.ProductCategories
{

    public class ProductCategory : AuditedAggregateRoot<Guid>
    { 
        public Guid ProductID { get; set; }

        public virtual Product Product { get; set; }

        //public int CategoryID { get; set; }
        public Guid CategoryID { get; set; }

        public virtual Category Category { get; set; }


    }
}