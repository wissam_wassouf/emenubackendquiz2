﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using Volo.Abp.Application.Dtos;

namespace Emenu.Products.Dto
{
    public class CreateUpdateProductDto : AuditedEntityDto<Guid>
    {
        [Required]
        [StringLength(200)]
        public string Name { get; set; }

        [StringLength(200)]
        public string Description { get; set; }

        public decimal? InventoryNumber { get; set; }

        public decimal? Price { get; set; }

        public decimal? Cost { get; set; }

        [Required]
        public string Code { get; set; }

       // public List<CreateUpdateProductImageDto> ProductImagessssss { get; set; }

     //   public List<Guid> CategoryIds { get; set; }
    }
}
