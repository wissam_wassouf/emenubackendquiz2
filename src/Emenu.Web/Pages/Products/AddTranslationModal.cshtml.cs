using AutoMapper.Internal.Mappers;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;
using System;
using Volo.Abp.AspNetCore.Mvc.UI.Bootstrap.TagHelpers.Form;
using Volo.Abp.Localization;
using Emenu.Products;
using System.Linq;
using Emenu.Web.Pages;
using Emenu.Products.Dto;

namespace Emenu.Web.Pages.Products;
public class AddTranslationModal : EmenuPageModel
{
    [HiddenInput]
    [BindProperty(SupportsGet = true)]
    public Guid Id { get; set; }

    public List<SelectListItem> Languages { get; set; }

    [BindProperty]
    public ProductTranslationViewModel TranslationViewModel { get; set; }

    private readonly IProductAppService _productAppService;
    private readonly ILanguageProvider _languageProvider;

    public AddTranslationModal(
        IProductAppService productAppService,
        ILanguageProvider languageProvider)
    {
        _productAppService = productAppService;
        _languageProvider = languageProvider;
    }

    public async Task OnGetAsync()
    {
        Languages = await GetLanguagesSelectItem();

        TranslationViewModel = new ProductTranslationViewModel();
    }

    public async Task<IActionResult> OnPostAsync()
    {
        await _productAppService.AddTranslationsAsync(Id, ObjectMapper.Map<ProductTranslationViewModel, AddProductTranslationDto>(TranslationViewModel));

        return NoContent();
    }

    private async Task<List<SelectListItem>> GetLanguagesSelectItem()
    {
        var result = await _languageProvider.GetLanguagesAsync();

        return result.Select(
            languageInfo => new SelectListItem
            {
                Value = languageInfo.CultureName,
                Text = languageInfo.DisplayName + " (" + languageInfo.CultureName + ")"
            }
        ).ToList();
    }

    public class ProductTranslationViewModel
    {
        [Required]
        [SelectItems(nameof(Languages))]
        public string Language { get; set; }

        [Required]
        public string Name { get; set; }

    }
}