using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using Emenu.Models.ProductCategories;
using Volo.Abp.Domain.Entities.Auditing;

namespace Emenu.Models.Categories
{

    public class Category : AuditedAggregateRoot<Guid>
    {

        public Category()
        {
            ProductCategories = new HashSet<ProductCategory>();
        }

        [Required]
        [StringLength(200)]
        public string Name { get; set; }

        [StringLength(1000)]
        public string Description { get; set; }

        public virtual ICollection<ProductCategory> ProductCategories { get; set; }

    }

}