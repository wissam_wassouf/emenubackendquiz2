﻿using Emenu.MultiLingualObjects;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Emenu.Products.Dto
{
  
    public class AddProductTranslationDto : IObjectTranslation
    {
        [Required]
        public string Language { get; set; }

        [Required]
        public string Name { get; set; }
    }

}
