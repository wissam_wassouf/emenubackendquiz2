﻿using Emenu.Localization;
using Volo.Abp.AspNetCore.Mvc.UI.RazorPages;

namespace Emenu.Web.Pages;

/* Inherit your PageModel classes from this class.
 */
public abstract class EmenuPageModel : AbpPageModel
{
    protected EmenuPageModel()
    {
        LocalizationResourceType = typeof(EmenuResource);
    }
}
