﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.Domain.Entities.Auditing;

namespace Emenu.Models.MultiLanguage
{
    public class SystemTable : AuditedAggregateRoot<Guid>
    {
        public SystemTable()
        {
            SysTableFileds = new HashSet<SystemTableFiled>();
        }
    
        public String Name { get; set; }

        public virtual ICollection<SystemTableFiled> SysTableFileds { get; set; }

    }
}
