using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using Emenu.Models.ProductCategories;
using Emenu.MultiLingualObjects;
using Volo.Abp.Domain.Entities.Auditing;

namespace Emenu.Models.Products
{

    public class Product : AuditedAggregateRoot<Guid>, IMultiLingualObject<ProductTranslation>
    {
        public Product()
        {
            ProductImages = new HashSet<ProductImage>();
            ProductCategories = new HashSet<ProductCategory>();
        }

        public string Name { get; set; }

        public string Description { get; set; }

        public decimal? InventoryNumber { get; set; }

        public decimal? Price { get; set; }

        public decimal? Cost { get; set; }

        public string Code { get; set; }

        public virtual ICollection<ProductImage> ProductImages { get; set; }

        public virtual ICollection<ProductCategory> ProductCategories { get; set; }

        public ICollection<ProductTranslation> Translations { get; set; }
    }
}