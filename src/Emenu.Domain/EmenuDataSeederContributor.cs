﻿using Emenu.Models.Categories;
using Emenu.Models.ProductCategories;
using Emenu.Models.Products;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp;
using Volo.Abp.Data;
using Volo.Abp.DependencyInjection;
using Volo.Abp.Domain.Repositories;

namespace Emenu
{
    public class EmenuDataSeederContributor : IDataSeedContributor, ITransientDependency
    {
        private readonly IRepository<Product, Guid> _productRepository;
        private readonly IRepository<Category, Guid> _categoryRepository;
        private readonly IRepository<ProductImage, Guid> _productImageRepository;
        private readonly IRepository<ProductCategory, Guid> _productCategoryRepository;

        public EmenuDataSeederContributor
            (IRepository<Product, Guid> productRepository,
              IRepository<Category, Guid> categoryRepository,
              IRepository<ProductImage, Guid> productImageRepository,
              IRepository<ProductCategory, Guid> productCategoryRepository)
        {
            _productRepository = productRepository;
            _categoryRepository = categoryRepository;
            _productImageRepository = productImageRepository;
            _productCategoryRepository = productCategoryRepository;
        }

        public async Task SeedAsync(DataSeedContext context)
        {

            await SeedCategories();
            await SeedProducts();
            await SeedProductImages();
            await SeedProductCategories();

        }

        private async Task SeedCategories()
        {
            // Check if categories already exist in the database
            if (await _categoryRepository.GetCountAsync() > 0)
            {
                return; // Data has already been seeded
            }

            // Create and add sample categories
            var categories = new List<Category>
            {
                new Category { Name = "Category 1" ,
                               Description = "Description For Category 1"},
                new Category { Name = "Category 2"  ,
                               Description = "Description For Category 2"},
           
                // Add more categories as needed
            };

            await _categoryRepository.InsertManyAsync(categories,
                autoSave: true);
        }



        private async Task SeedProducts()
        {
            // Check if products already exist in the database
            if (await _productRepository.GetCountAsync() > 0)
            {
                return; // Data has already been seeded
            }

            var product1 = new Product
            {

                Name = "Product 1",
                Description = "Description 1",
                InventoryNumber = 10,
                Price = 9.99m,
                Cost = 5.99m,
                Code = "P1"
            };
            await _productRepository.InsertAsync(product1);

            var product2 = new Product
            {

                Name = "Product 2",
                Description = "Description 2",
                InventoryNumber = 5,
                Price = 19.99m,
                Cost = 15.99m,
                Code = "P2"
            };
            await _productRepository.InsertAsync(product2 , autoSave: true);

        }

        private async Task SeedProductImages()
        {
            // Check if product images already exist in the database
            if (await _productImageRepository.GetCountAsync() > 0)
            {
                return; // Data has already been seeded
            }

            // Get the products for referencing in product images
            var products = await _productRepository.GetListAsync();

            // Create and add sample product images
            var productImages = new List<ProductImage>
               {
                   new ProductImage
                   {
                       ImageUrl = "https://example.com/image1.jpg",
                       ProductID = products.FirstOrDefault(p => p.Code == "P1")?.Id ?? Guid.Empty
                   },
                   new ProductImage
                   {
                        ImageUrl = "https://example.com/image2.jpg",
                        ProductID = products.FirstOrDefault(p => p.Code == "P2")?.Id ?? Guid.Empty
                    },
                        // Add more product images as needed
                };

           await _productImageRepository.InsertManyAsync(productImages , autoSave: true);

        }

        private async Task SeedProductCategories()
        {
            /*   var product1 = await _productRepository.FirstOrDefaultAsync(*//* predicate *//*);
               var category1 = await _categoryRepository.FirstOrDefaultAsync(*//* predicate *//*);

               if (product1 != null && category1 != null)
               {
                   // Create the many-to-many relationship
                 //  product1.Categories.Add(new ProductCategory { ProductId = product1.Id, CategoryId = category1.Id });
                 //  await CurrentUnitOfWork.SaveChangesAsync();
               }*/

            // Check if product categories already exist in the database
            if (await _productCategoryRepository.GetCountAsync() > 0)
            {
                return; // Data has already been seeded
            }

            // Get the products and categories for referencing in product categories
            var products = await _productRepository.GetListAsync();
            var categories = await _categoryRepository.GetListAsync();

            // Create and add sample product categories
            var productCategories = new List<ProductCategory>
            {
                new ProductCategory
                {
                    ProductID =  products.FirstOrDefault(p => p.Code == "P1")?.Id ?? Guid.Empty,
                    CategoryID = categories.FirstOrDefault(c => c.Name == "Category 1")?.Id ?? Guid.Empty
                },
                new ProductCategory
                {
                    ProductID = products.FirstOrDefault(p => p.Code == "P2")?.Id ?? Guid.Empty,
                    CategoryID = categories.FirstOrDefault(c => c.Name == "Category 2")?.Id ?? Guid.Empty
                },
                // Add more product categories as needed
            };

           await _productCategoryRepository.InsertManyAsync(productCategories, autoSave: true);

        }

    }

}
