﻿using System.Threading.Tasks;

namespace Emenu.Data;

public interface IEmenuDbSchemaMigrator
{
    Task MigrateAsync();
}
