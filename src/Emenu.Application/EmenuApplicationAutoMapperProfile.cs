﻿using AutoMapper;
using Emenu.Models.Products;
using Emenu.ProductImages.Dto;
using Emenu.Products.Dto;

namespace Emenu;

public class EmenuApplicationAutoMapperProfile : Profile
{
    public EmenuApplicationAutoMapperProfile()
    {
        CreateMap<Product, Products.Dto.ProductDto>();

        CreateMap<CreateUpdateProductDto, Product>()
          // .ForMember(dest => dest.ProductImages, opt => opt.Ignore()) // Ignore mapping images
          // .ForMember(dest => dest.ProductCategories, opt => opt.Ignore()) // Ignore mapping categories
           .ReverseMap();

        CreateMap<CreateUpdateProductImageDto, ProductImage>()
    //        .ForMember(dest => dest.Product, opt => opt.Ignore()) // Ignore mapping product
    //        .ForMember(dest => dest.ImageUrl, opt => opt.Ignore()) // Ignore mapping URL
            .ReverseMap();

        CreateMap<ProductImage, ProductImages.Dto.ProductImageDto>();


        /* You can configure your AutoMapper mapping configuration here.
         * Alternatively, you can split your mapping configurations
         * into multiple profile classes for a better organization. */
    }
}
