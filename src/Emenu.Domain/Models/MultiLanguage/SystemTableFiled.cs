﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.Domain.Entities.Auditing;

namespace Emenu.Models.MultiLanguage
{
    public class SystemTableFiled : AuditedAggregateRoot<Guid>
    {
        public SystemTableFiled()
        {
            SysTabelFiledLanguages = new HashSet<SystemTableFiledLanguage>();

        }

        [StringLength(200)]
        public String Name { get; set; }

        public Guid SystemTableID { get; set; }

        public virtual SystemTable SystemTable { get; set; }
        public virtual ICollection<SystemTableFiledLanguage> SysTabelFiledLanguages { get; set; }

    }
}
