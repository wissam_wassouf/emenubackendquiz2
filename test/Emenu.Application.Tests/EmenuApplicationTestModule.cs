﻿using Volo.Abp.Modularity;

namespace Emenu;

[DependsOn(
    typeof(EmenuApplicationModule),
    typeof(EmenuDomainTestModule)
    )]
public class EmenuApplicationTestModule : AbpModule
{

}
