﻿using Emenu.ProductImages.Dto;
using Emenu.Products.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.Application.Services;

namespace Emenu.Products
{
    public interface IProductService : IApplicationService
    {
         Task<Guid> Create(Dto.ProductDto input);
      
        Task<Guid> UploadProductImage(ProductImages.Dto.ProductImageDto input);

        Task DeleteProductImage(Guid productImageId);
    }
}
