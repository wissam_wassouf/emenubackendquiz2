﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;
using Volo.Abp.Application.Dtos;

namespace Emenu.Products.Dto
{
    public class CreateUpdateProductImageDto : AuditedEntityDto<Guid>
    {
        public IFormFile File { get; set; }
    }
}
