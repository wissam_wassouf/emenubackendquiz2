﻿using Emenu.CloudinaryServices;
using Emenu.Models.MultiLanguage;
using Emenu.Models.Products;
using Emenu.ProductImages.Dto;
using Emenu.Products.Dto;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Linq.Expressions;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using Volo.Abp;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Application.Services;
using Volo.Abp.Domain.Entities;
using Volo.Abp.Domain.Repositories;
using Volo.Abp.ObjectMapping;

namespace Emenu.Products
{

    public class ProductAppService :
    CrudAppService<
        Product, //The Product entity
        ProductDto, //Used to show Products
        Guid, //Primary key of the Product entity
        PagedAndSortedResultRequestDto, //Used for paging/sorting
        CreateUpdateProductDto>, //Used to create/update a Product
    IProductAppService //implement the IProductAppService
    {
        private readonly IRepository<ProductImage, Guid> _productImageRepository;
        private readonly IRepository<Product, Guid> _productRepository;
        private readonly ICloudinaryService _cloudinaryService;

        public ProductAppService(
            IRepository<Product, Guid> repository,
            IRepository<ProductImage, Guid> productImageRepository,
            IRepository<Product, Guid> productRepository)
            : base(repository)
        {
            //  _authorRepository = authorRepository;
            _productImageRepository = productImageRepository;
            _productRepository = productRepository;
            ICloudinaryService cloudinaryService;
        }

        public override async Task<ProductDto> GetAsync(Guid id)
        {
            // Get the IQueryable<Product> from the repository
            var queryable = await Repository.GetQueryableAsync();

            // Prepare a query to join Products and ProductImages
            var query = from product in queryable
                        join productImage in await _productImageRepository.GetQueryableAsync() on product.Id equals productImage.ProductID
                        where product.Id == id
                        select new { Product = product, ProductImage = productImage };

            // Execute the query and get the Product with ProductImages
            var queryResults = await AsyncExecuter.ToListAsync(query);
            if (queryResults.Count == 0)
            {
                throw new EntityNotFoundException(typeof(Product), id);
            }

            var productDto = ObjectMapper.Map<Product, ProductDto>(queryResults[0].Product);
            productDto.ProductImages = queryResults.Select(result => ObjectMapper.Map<ProductImage, ProductImageDto>(result.ProductImage)).ToList();

            return productDto;
        }


        public override async Task<PagedResultDto<ProductDto>> GetListAsync(PagedAndSortedResultRequestDto input)
        {
            // Get the IQueryable<Product> from the repository
            var queryable = await Repository.GetQueryableAsync();

            // Prepare a query to join Products and ProductImages
            var query = from product in queryable
                        join productImage in await _productImageRepository.GetQueryableAsync() on product.Id equals productImage.ProductID
                        select new { Product = product, ProductImage = productImage };


            // Paging
            query = query
                .OrderBy(NormalizeSorting(input.Sorting))
                .Skip(input.SkipCount)
                .Take(input.MaxResultCount);

            // Execute the query and get a list
            var queryResult = await AsyncExecuter.ToListAsync(query);

            // Convert the query result to a list of ProductDto objects
            var productDtos = queryResult.Select(x =>
            {
                var productDto = ObjectMapper.Map<Product, ProductDto>(x.Product);

                // You might also want to map the ProductImage data here if needed

                return productDto;
            }).ToList();

            // Get the total count with another query
            var totalCount = await Repository.GetCountAsync();

            return new PagedResultDto<ProductDto>(
                totalCount,
                productDtos
            );
        }

        private static string NormalizeSorting(string sorting)
        {
            if (sorting.IsNullOrEmpty())
            {
                return $"Product.{nameof(Product.Name)}";
            }

            return $"Product.{sorting}";
        }

        private static string NormalizeSortingV2(string sorting)
        {
            if (string.IsNullOrWhiteSpace(sorting))
            {
                // Default sorting if not provided
                return "Name"; // You can change this to any default property
            }

            var sortExpressions = sorting.Split(',');
            var normalizedSortExpressions = new List<string>();

            foreach (var sortExpression in sortExpressions)
            {
                var trimmedSortExpression = sortExpression.Trim();
                var isDescending = false;

                if (trimmedSortExpression.EndsWith(" desc", StringComparison.OrdinalIgnoreCase))
                {
                    isDescending = true;
                    trimmedSortExpression = trimmedSortExpression.Substring(0, trimmedSortExpression.Length - 5).Trim();
                }
                else if (trimmedSortExpression.EndsWith(" asc", StringComparison.OrdinalIgnoreCase))
                {
                    trimmedSortExpression = trimmedSortExpression.Substring(0, trimmedSortExpression.Length - 4).Trim();
                }

                // Add sorting expression to the list
                var normalizedSort = isDescending ? $"{trimmedSortExpression} descending" : trimmedSortExpression;
                normalizedSortExpressions.Add(normalizedSort);
            }

            // Combine the normalized sort expressions into a single string
            return string.Join(", ", normalizedSortExpressions);
        }

        public async Task AddTranslationsAsync(Guid id, AddProductTranslationDto input)
        {
            var queryable = await Repository.WithDetailsAsync();

            var product = await AsyncExecuter.FirstOrDefaultAsync(queryable, x => x.Id == id);

            if (product.Translations.Any(x => x.Language == input.Language))
            {
                throw new UserFriendlyException($"Translation already available for {input.Language}");
            }
            try
            {
                product.Translations.Add(new ProductTranslation
            {
                ProductId = product.Id,
                Name = input.Name,
                Language = input.Language
            });

            await Repository.UpdateAsync(product);
            }
            catch (Exception exception)
            {
                while (exception.InnerException != null)
                {
                    exception = exception.InnerException;
                }
               // return Json(exception.Message);
            }
        }




    }
}
