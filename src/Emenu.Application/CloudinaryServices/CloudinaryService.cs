﻿using CloudinaryDotNet;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Http;
using CloudinaryDotNet.Actions;

namespace Emenu.CloudinaryServices
{
    public class CloudinaryService : ICloudinaryService
    {

        private readonly Cloudinary _cloudinary;

        public CloudinaryService(IConfiguration configuration)
        {
            var cloudinaryConfig = configuration.GetSection("Cloudinary");
            var account = new Account(
                cloudinaryConfig["CloudName"],
                cloudinaryConfig["ApiKey"],
                cloudinaryConfig["ApiSecret"]);

            _cloudinary = new Cloudinary(account);
        }



        public async Task DeleteImageAsync(string imageUrl)
        {
            // Implement Cloudinary delete logic here
            // Use Cloudinary SDK to delete the image using its URL
            var deleteParams = new DeletionParams(imageUrl);
            await _cloudinary.DestroyAsync(deleteParams);
        }

        public async Task<string> UploadImageAsync(IFormFile file)
        {
            if (file == null || file.Length <= 0)
                throw new ArgumentException("Invalid file");

            using var stream = file.OpenReadStream();

            var uploadParams = new ImageUploadParams
            {
                File = new FileDescription(file.FileName, stream),
                Transformation = new Transformation().Crop("fill").Gravity("face")
            };

            var uploadResult = await _cloudinary.UploadAsync(uploadParams);

            return uploadResult.SecureUrl.AbsoluteUri;
        }
    }
}
