﻿using System.Threading.Tasks;
using Shouldly;
using Xunit;

namespace Emenu.Pages;

public class Index_Tests : EmenuWebTestBase
{
    [Fact]
    public async Task Welcome_Page()
    {
        var response = await GetResponseAsStringAsync("/");
        response.ShouldNotBeNull();
    }
}
