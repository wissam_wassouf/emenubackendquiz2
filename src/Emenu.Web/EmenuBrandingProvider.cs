﻿using Volo.Abp.Ui.Branding;
using Volo.Abp.DependencyInjection;

namespace Emenu.Web;

[Dependency(ReplaceServices = true)]
public class EmenuBrandingProvider : DefaultBrandingProvider
{
    public override string AppName => "Emenu";
}
