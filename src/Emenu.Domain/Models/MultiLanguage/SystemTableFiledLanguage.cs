﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.Domain.Entities.Auditing;

namespace Emenu.Models.MultiLanguage
{
    public class SystemTableFiledLanguage : AuditedAggregateRoot<Guid>
    {
     
       // public int SystemTablelFiledID { get; set; }
        public Guid SystemTablelFiledID { get; set; }

        public virtual SystemTableFiled SystemTablelFiled { get; set; }

        public Guid LanguageID { get; set; }

        public virtual Language Language { get; set; }
    }
}
