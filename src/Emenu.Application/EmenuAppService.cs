﻿using System;
using System.Collections.Generic;
using System.Text;
using Emenu.Localization;
using Volo.Abp.Application.Services;

namespace Emenu;

/* Inherit your application services from this class.
 */
public abstract class EmenuAppService : ApplicationService
{
    protected EmenuAppService()
    {
        LocalizationResource = typeof(EmenuResource);
    }
}
