
using Emenu.Models.Products;
using Emenu.MultiLingualObjects;
using Emenu.Products.Dto;
using Volo.Abp.DependencyInjection;
using Volo.Abp.Localization;
using Volo.Abp.ObjectMapping;
using Volo.Abp.Settings;
using Volo.Abp.Threading;

namespace Acme.BookStore;

public class MultiLingualProductObjectMapper : IObjectMapper<Product, ProductDto>, ITransientDependency
{
    private readonly MultiLingualObjectManager _multiLingualObjectManager;

    private readonly ISettingProvider _settingProvider;

    public MultiLingualProductObjectMapper(
        MultiLingualObjectManager multiLingualObjectManager,
        ISettingProvider settingProvider)
    {
        _multiLingualObjectManager = multiLingualObjectManager;
        _settingProvider = settingProvider;
    }

    public ProductDto Map(Product source)
    {
        var translation = AsyncHelper.RunSync(() =>
            _multiLingualObjectManager.FindTranslationAsync<Product, ProductTranslation>(source));

        return new ProductDto
        {
            Id = source.Id,
            Code = source.Code,
            Name = translation?.Name ?? source.Name,
            Price = source.Price,
            InventoryNumber = source.InventoryNumber,
            Cost = source.Cost,
            Language = translation?.Language ?? AsyncHelper.RunSync(() => _settingProvider.GetOrNullAsync(LocalizationSettingNames.DefaultLanguage)),
            CreationTime = source.CreationTime,
            CreatorId = source.CreatorId,
            LastModificationTime = source.LastModificationTime,
            LastModifierId = source.LastModifierId
        };
    }

    public ProductDto Map(Product source, ProductDto destination)
    {
        return default;
    }
}