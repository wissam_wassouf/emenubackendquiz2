﻿using Emenu.ProductImages.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.Application.Services;

namespace Emenu.ProductImages
{
    public interface IProductImageAppService : IApplicationService
    {
        Task<ProductImageDto> CreateAsync(ProductImageCreateDto input);
        Task<ProductImageDto> GetAsync(Guid id);
        Task<ProductImageDto> UpdateAsync(Guid id, ProductImageUpdateDto input);
        Task DeleteAsync(Guid id);
    }
}
